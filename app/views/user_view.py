from flask import Blueprint, request, current_app, jsonify
from app.configs import jwt
from app.models.user_model import UserModel
from werkzeug.security import generate_password_hash, check_password_hash
from http import HTTPStatus
import secrets
from flask_httpauth import HTTPAuth, HTTPTokenAuth
from flask_jwt_extended import create_access_token, jwt_required


bp = Blueprint('users_bp', __name__, url_prefix='/api')
auth = HTTPTokenAuth(scheme='Bearer')

@bp.post('/signup')
def create():
    data = request.json

    password_to_hash = data.pop('password')

    new_user = UserModel(**data)
    new_user.password = password_to_hash

    session = current_app.db.session

    session.add(new_user)
    session.commit()

    return jsonify(new_user), HTTPStatus.ACCEPTED
             

@bp.post('/signin')
def login():
    data = request.json

    found_user: UserModel = UserModel.query.filter_by(email=data['email']).first()

    if not found_user:
        return {"message": "User not found"}, HTTPStatus.NOT_FOUND
    
    if not found_user.verify_password(data['password']):
        return {'message': 'Unauthorized'}, HTTPStatus.UNAUTHORIZED
    
    access_token = create_access_token(identity=found_user)
    return {'access_token': access_token}, HTTPStatus.OK
    

@bp.get('/')
@jwt_required()
def get_users():
    return jsonify(UserModel.query.all()), HTTPStatus.OK


@bp.put('')
@jwt_required()
def update_user():

    data = request.json

    found_user: UserModel = UserModel.query.filter_by(email=data['email']).first()

    if not found_user:
        return {"message": "User not found"}, HTTPStatus.NOT_FOUND
    
    if not found_user.verify_password(data['password']):
        return {'message': 'Unauthorized'}, HTTPStatus.UNAUTHORIZED

    data.pop('password')

    UserModel.query.filter_by(email=data['email']).update(data)
    
    session = current_app.db.session
    session.commit() 

    output = UserModel.query.get(found_user.id)

    return jsonify(output), HTTPStatus.OK


@bp.delete('')
@jwt_required()
def delete_user():
    data = request.json

    found_user: UserModel = UserModel.query.filter_by(email=data['email']).first()

    if not found_user:
        return {"message": "User not found"}, HTTPStatus.NOT_FOUND
    
    if not found_user.verify_password(data['password']):
        return {'message': 'Unauthorized'}, HTTPStatus.UNAUTHORIZED

    session = current_app.db.session
    session.delete(found_user)
    session.commit()

    return jsonify(found_user), HTTPStatus.OK