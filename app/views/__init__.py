from flask import Flask

from .user_view import bp as bp_users

def init_app(app: Flask):
    app.register_blueprint(bp_users)